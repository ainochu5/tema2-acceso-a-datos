SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

CREATE DATABASE IF NOT EXISTS `gestorfacturas` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `gestorfacturas`;

DELIMITER $$
DROP PROCEDURE IF EXISTS `aplicar_descuento_productos`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `aplicar_descuento_productos`()
BEGIN
	UPDATE productos
	SET precio=precio-(precio*0.20)
	WHERE descuento=1;
END$$

DROP PROCEDURE IF EXISTS `eliminar_tipo_cliente`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `eliminar_tipo_cliente`(IN `p_tipo_cliente` VARCHAR(50))
BEGIN
		DELETE FROM clientes WHERE tipo_cliente=p_tipo_cliente;
END$$

DROP FUNCTION IF EXISTS `get_cantidad_productos_descuento`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_cantidad_productos_descuento`() RETURNS int(11)
BEGIN
	DECLARE cantidad INT;
	 SET cantidad=(SELECT COUNT(*) FROM productos WHERE descuento = 1);
     RETURN cantidad;
END$$

DROP FUNCTION IF EXISTS `get_domicilio_factura`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_domicilio_factura`(`nombre_des` VARCHAR(255)) RETURNS varchar(255) CHARSET latin1
    NO SQL
BEGIN
	DECLARE domicilio_des VARCHAR(50);
	 SET domicilio_des =(SELECT domicilio FROM clientes WHERE nombre = nombre_des);
     RETURN domicilio_des;
END$$

DROP FUNCTION IF EXISTS `get_id_producto`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_id_producto`(`p_id_factura` INT) RETURNS int(11)
    NO SQL
BEGIN
	DECLARE id_articulo INT;
	 SET id_articulo=(SELECT id_producto FROM factura_carrito WHERE id_factura=p_id_factura);
     RETURN id_articulo;
END$$

DROP FUNCTION IF EXISTS `get_total_carrito`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `get_total_carrito`(`f_id` INT) RETURNS float
    NO SQL
BEGIN
	DECLARE total float;
	 SET total=(SELECT SUM(FC.total) FROM productos P, facturas F, factura_carrito FC WHERE FC.id_factura=f.id AND FC.id_producto = P.id AND f_id = FC.id_factura);
     RETURN total;
END$$

DELIMITER ;

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(10) unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellidos` varchar(255) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `domicilio` varchar(50) NOT NULL,
  `tipo_cliente` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `facturas`;
CREATE TABLE IF NOT EXISTS `facturas` (
  `id` int(10) unsigned NOT NULL,
  `domicilio` varchar(50) NOT NULL,
  `fecha_factura` date NOT NULL,
  `destinatario_factura` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `factura_carrito`;
CREATE TABLE IF NOT EXISTS `factura_carrito` (
  `id_factura` int(10) unsigned NOT NULL DEFAULT '0',
  `id_producto` int(10) unsigned NOT NULL DEFAULT '0',
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `login`;
CREATE TABLE IF NOT EXISTS `login` (
  `id` int(10) unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `rol_usuario` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `productos`;
CREATE TABLE IF NOT EXISTS `productos` (
  `id` int(10) unsigned NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `descripcion` varchar(255) NOT NULL,
  `precio` float DEFAULT NULL,
  `descuento` tinyint(1) NOT NULL,
  `cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `facturas`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `factura_carrito`
  ADD PRIMARY KEY (`id_factura`,`id_producto`),
  ADD KEY `id_factura` (`id_factura`),
  ADD KEY `id_producto` (`id_producto`);

ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `clientes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `facturas`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `login`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
ALTER TABLE `productos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;

ALTER TABLE `factura_carrito`
  ADD CONSTRAINT `factura_carrito_ibfk_1` FOREIGN KEY (`id_factura`) REFERENCES `facturas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `factura_carrito_ibfk_2` FOREIGN KEY (`id_producto`) REFERENCES `productos` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*INSERTAR DATOS EN LA TABLA LOGIN*/
INSERT INTO `login` (`id`, `nombre`, `password`, `rol_usuario`) VALUES
(1, 'ainoa', 'andres', 'Admin'),
(2, 'cristian', 'granged', 'Local user');
  
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
