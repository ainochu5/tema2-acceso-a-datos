package com.ainochu.gestor_facturas.Main.util;

import java.io.File;

/**
 * Clase que contiene las constantes del programa
 */
public class Constantes {

    public static String RUTAXMLCLIENTES = System.getProperty("user.home")+File.separator+"archivoCliente.xml";
    public static String RUTAXMLFACTURAS = System.getProperty("user.home")+File.separator+"archivoFactura.xml";
    public static String RUTAXMLPRODUCTOS = System.getProperty("user.home")+File.separator+"archivoArticulo.xml" ;
    public static String RUTAXMLCARRITO = System.getProperty("user.home")+File.separator+"archivoCarrito.xml" ;
    public static String RUTAARCHIVOCONF = System.getProperty("user.home") +
            File.separator + "archivo.conf";
    public static String NOMBREBD = "gestorfacturas";
    public static String RUTABD = "localhost";
    public static String TIPOCONEXION = "mysql";

}
