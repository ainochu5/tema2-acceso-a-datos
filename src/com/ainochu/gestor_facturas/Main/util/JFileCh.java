package com.ainochu.gestor_facturas.Main.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;
import javax.swing.*;
import static com.ainochu.gestor_facturas.Main.util.Constantes.*;
/**
 * Created by Ainoa on 05/11/2015.
 */
public class JFileCh {

    private JTextField ruta;
    private File fichero;



    public void escribirFichero(){
        File archivo = new File(RUTAARCHIVOCONF);

        Properties configuracion = new Properties();
        //ASIGNAR VALORES AL FICHERO
        configuracion.setProperty("Ruta_BD", RUTABD);
        configuracion.setProperty("Nombre_BD", NOMBREBD);
        configuracion.setProperty("Tipo_BD", TIPOCONEXION);
        //GUARDAR EL ARCHIVO DE CONFIGURACION (NOMBRE DEL FICHERO, COMENTARIO)
        try {
            configuracion.store(new FileOutputStream(archivo),"Fichero configuracion");
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
    }
    public void abrirArchivoConf(){
        File fichero = new File("archivo.txt");
        if(fichero.exists()) {
            Properties properties = new Properties();
            try {
                properties.load(new FileInputStream(RUTAARCHIVOCONF));
                RUTABD = properties.getProperty("Ruta_BD");
                NOMBREBD = properties.getProperty("Nombre_BD");
                TIPOCONEXION = properties.getProperty("Tipo_BD");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



}
