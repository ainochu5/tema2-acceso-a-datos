package com.ainochu.gestor_facturas.Main.util;
import com.ainochu.gestor_facturas.Main.Class.Articulo;
import com.ainochu.gestor_facturas.Main.Class.Factura;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

public class Excel {
    private Factura f1;
    private ArrayList<Articulo>listaFactura;
    public Excel(ArrayList<Articulo> listaFacturas, Factura factura){
        f1 = factura;
        listaFactura = listaFacturas;
    }

    /**
     * Metodo que se encarga de lanzar la nueva hoja de excel, asignando
     * las filas y columnas. Primero creamos las columnas. Dentro de cada columna creamos x celdas
     * (en nuestro caso 4) y despues rellenamos cada una de las celdas con los
     * los atributos del array facturas.
     */
    public void cargar() throws IOException{
        String rutaArchivo = System.getProperty("user.home")+"/factura.xls";
        File archivoXLS = new File(rutaArchivo);
        archivoXLS.createNewFile();
        Workbook libro = new HSSFWorkbook();
        FileOutputStream archivo = new FileOutputStream(archivoXLS);
        Sheet hoja = libro.createSheet("Factura ");
        System.out.print(listaFactura.size());
        int cont = 0;
        for(int f=0;f<listaFactura.size()+1;f++) {
            //CREAMOS LAS FILAS
            Row columna1 = hoja.createRow(f);
            Row columna2 = hoja.createRow(f);
            Row columna3 = hoja.createRow(f);
            Row columna4 = hoja.createRow(f);

            //CADA FILA TENDRA 4 CELDAS

                org.apache.poi.ss.usermodel.Cell celda1 = columna1.createCell(0);
                org.apache.poi.ss.usermodel.Cell celda2 = columna2.createCell(1);
                org.apache.poi.ss.usermodel.Cell celda3 = columna3.createCell(2);
                org.apache.poi.ss.usermodel.Cell celda4 = columna4.createCell(3);


                /*Si la fila es la numero 0, estableceremos los encabezados*/
                if (f == 0) {
                    celda1.setCellValue("Nombre");
                    celda2.setCellValue("Id producto");
                    celda3.setCellValue("Cantidad");
                    celda4.setCellValue("Precio");
                } else {
                    //sino rellenamos con la tabla
                        celda1.setCellValue(listaFactura.get(cont).getNombreArticulo());
                    System.out.printf("hola");
                        celda2.setCellValue(listaFactura.get(cont).getDescripcion());
                        celda3.setCellValue(listaFactura.get(cont).getCantidadArticulos());
                        celda4.setCellValue(listaFactura.get(cont).getPrecioArticulo());
                    cont++;
                }
        }
        datosCliente(hoja);
        libro.write(archivo);
        archivo.close();
        Desktop.getDesktop().open(archivoXLS);
    }

    /**
     * Metodo que se encarga de guardar otros datos tales como: nombre cliente y domicilio.
     */
    private void datosCliente(Sheet hoja){
        Row columna1 = hoja.createRow(listaFactura.size() + 2);
        Row columna2 = hoja.createRow(listaFactura.size() + 2);
        org.apache.poi.ss.usermodel.Cell celda1 = columna1.createCell(0);
        org.apache.poi.ss.usermodel.Cell celda2 = columna2.createCell(1);
        celda1.setCellValue("Nombre Cliente: ");
        celda2.setCellValue(f1.getDestinatarioFactura());
        Row columna3 = hoja.createRow(listaFactura.size()+3);
        Row columna4 = hoja.createRow(listaFactura.size()+3);
        org.apache.poi.ss.usermodel.Cell celda3 = columna3.createCell(0);
        org.apache.poi.ss.usermodel.Cell celda4 = columna4.createCell(1);
        celda3.setCellValue("Domicilio cliente: ");
        celda4.setCellValue(f1.getDomicilioFactura());
    }
}
