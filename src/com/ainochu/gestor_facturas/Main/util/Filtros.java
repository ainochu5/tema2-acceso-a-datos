package com.ainochu.gestor_facturas.Main.util;

import javax.swing.filechooser.FileFilter;
import java.io.File;

/**
 * Filtro que se carga para seleccionar unicamente ficheros .xml
 */
public class Filtros extends FileFilter {

    @Override
    public boolean accept(File fichero) {
        if (fichero.getName().endsWith(".xml"))
            return true;

        if (fichero.isDirectory()) {
            return true;
        }

        return false;
    }

    @Override
    public String getDescription() {
        return "Ficheros XML";
    }
}
