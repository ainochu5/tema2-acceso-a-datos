package com.ainochu.gestor_facturas.Main.util;

import javax.swing.*;
import java.io.*;

/**
 * Created by Ainoa on 30/10/2015.
 */
public class Util {
    /**
     * Metodo encargado de abrir los archivos: clientes, articulos y facturas
     */

    public static Object cargarArchivo(String nombre_Archivo)
            throws IOException, ClassNotFoundException{
        ObjectInputStream entrada = new ObjectInputStream(new FileInputStream(nombre_Archivo));
        Object objeto = entrada.readObject();
        return objeto;
    }

    /**
     * Metodo encargado de guardar todos los cambios sobre: clientes,articulos y facturas
     */
    public static void guardarArchivo(String nombre_Archivo, Object objeto)
            throws IOException, ClassNotFoundException{
        ObjectOutputStream salida = new ObjectOutputStream(new FileOutputStream(nombre_Archivo));
        salida.writeObject(objeto);
    }

    /**
     * Metodo que se encargar de mostrar un mensaje informativo. Recibe como parametro
     * el mensaje que quiere mostrar el usuario.
     */
    public static void mensajeConfirmacion(String mensaje){
        JOptionPane.showMessageDialog(null, mensaje);

    }

}
