package com.ainochu.gestor_facturas.Main.bean;

import com.ainochu.gestor_facturas.Main.Class.Factura;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;

/**
 * Created by Ainoa on 06/12/2015.
 */
public class TableFacturas extends JTable {

    private DefaultTableModel modelo;

    public TableFacturas(){
        modelo = new DefaultTableModel();
    }

    public void anadirColumnas(){
        modelo.addColumn("ID factura");
        modelo.addColumn("Domicilio");
        modelo.addColumn("F.Factura");
        modelo.addColumn("Destinatario factura");
        this.setModel(modelo);
    }
    public void rellenarTabla(ArrayList<Factura> listaFacturas){
        modelo.setNumRows(0);
        this.setModel(modelo);
        if(listaFacturas.size() == 0){
            System.out.println("Vacio");
            return;
        }
        for(int i=0; i<listaFacturas.size(); i++){
            String datos[] = {Integer.toString(listaFacturas.get(i).getIdFactura()),listaFacturas.get(i).getDomicilioFactura(),
                    listaFacturas.get(i).getFechaFactura().toString(),listaFacturas.get(i).getDestinatarioFactura()};
            modelo.addRow(datos);
        }
        this.setModel(modelo);
    }

    public Factura eliminarFactura (ArrayList<Factura> listaFacturas){
        Factura f = listaFacturas.get(this.getSelectedRow());
        modelo.removeRow(this.getSelectedRow());
        this.setModel(modelo);
        return f;
    }

    public Factura cargarFactura(ArrayList<Factura> listaFacturas){
        Factura f = listaFacturas.get(this.getSelectedRow());
        return f;
    }
}
