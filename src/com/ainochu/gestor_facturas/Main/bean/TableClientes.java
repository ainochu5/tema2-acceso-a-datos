package com.ainochu.gestor_facturas.Main.bean;

import com.ainochu.gestor_facturas.Main.Class.Cliente;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;

/**
 * Created by Ainoa on 03/12/2015.
 */
public class TableClientes extends JTable{

    private DefaultTableModel modelo;

    public TableClientes(){
        modelo = new DefaultTableModel();
    }

    public void anadirColumnas(){
        modelo.addColumn("Nombre");
        modelo.addColumn("Apellidos");
        modelo.addColumn("F.Nacimiento");
        modelo.addColumn("Domicilio");
        modelo.addColumn("Tipo Cliente");
        this.setModel(modelo);
    }
    public void rellenarTabla(ArrayList<Cliente> listaClientes){
        modelo.setNumRows(0);
        this.setModel(modelo);
        if(listaClientes.size() == 0){
            System.out.println("Vacio");
            return;
        }
        for(int i=0; i<listaClientes.size(); i++){
            String datos[] = {listaClientes.get(i).getNombre(),listaClientes.get(i).getApellidos(),listaClientes.get(i).getFechaNacimiento().toString(),
            listaClientes.get(i).getDireccion(),listaClientes.get(i).getTipo_Cliente()};
            modelo.addRow(datos);
        }
        this.setModel(modelo);
    }


    public Cliente eliminarCliente(ArrayList<Cliente> listaClientes){
        Cliente c = listaClientes.get(this.getSelectedRow());
        modelo.removeRow(this.getSelectedRow());
        this.setModel(modelo);
        return c;
    }

    public Cliente cargarCliente(ArrayList<Cliente> listaClientes){
        Cliente c = listaClientes.get(this.getSelectedRow());
        return c;
    }


}
