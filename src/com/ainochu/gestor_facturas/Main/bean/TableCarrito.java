package com.ainochu.gestor_facturas.Main.bean;

import com.ainochu.gestor_facturas.Main.Class.Articulo;
import com.ainochu.gestor_facturas.Main.Class.Carrito;
import com.ainochu.gestor_facturas.Main.util.Util;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;

/**
 * JTable que almacena las facturas de cada cliente
 */
public class TableCarrito extends JTable{

    private ArrayList<Articulo>listaArticulos;
    private float precioTotal;
    private float cantidad;


    public TableCarrito(){

    }
    public TableCarrito(ArrayList<Articulo> listaArticulos){
        this.listaArticulos = listaArticulos;
    }


    /**
     * Metodo que se encarga de a�adir a la tabla aquellos articulos de una nueva factura
     */
    public void rellenarTabla(ArrayList<Articulo> listaFacturas){
        formatearTabla();
        DefaultTableModel dtm = new DefaultTableModel();
        this.setModel(dtm);
        dtm.addColumn("Nombre ");
        dtm.addColumn("id producto");
        dtm.addColumn("Precio ud.");
        dtm.addColumn("Precio total");
        for (int i = 0; i < listaFacturas.size(); i++){
            String datos[] = {listaFacturas.get(i).getNombreArticulo(), Integer.toString(listaFacturas.get(i).getId_articulo()), Float.toString(listaFacturas.get(i).getPrecioArticulo())};
            dtm.addRow(datos);
        }
    }

    /**
     * Metodo que se encarga de mostrar y rellenar la tabla del cliente seleccionado.
     */
    public float mostrarFacturasPorCliente(ArrayList<Articulo> listaFacturas, ArrayList<Carrito> listaArticulosPorCliente){
        formatearTabla();
        DefaultTableModel dtm = new DefaultTableModel();
        this.setModel(dtm);
        dtm.addColumn("Nombre ");
        dtm.addColumn("id producto");
        dtm.addColumn("Precio ud.");
        dtm.addColumn("Precio total");
        for (int i = 0; i < listaFacturas.size(); i++){
                    String datos[] = {listaFacturas.get(i).getNombreArticulo(), Integer.toString(listaFacturas.get(i).getId_articulo()), Float.toString(listaFacturas.get(i).getPrecioArticulo()),Float.toString(precioTotal)};
                    dtm.addRow(datos);
        }
        return precioTotal;
    }

    /**
     * Metodo que se encarga de formatear la tabla recorriendo las filas
     */
    public void formatearTabla(){
            try {
                DefaultTableModel modelo=(DefaultTableModel) this.getModel();
                int filas=this.getRowCount();
                for (int i = 0;filas>i; i++) {
                    modelo.removeRow(0);
                }
            } catch (Exception e) {
                Util.mensajeConfirmacion("Error al limpiar la tabla");
            }
        }

    /**
     * Metodo que se encarga de eliminar elementos de la tabla de facturas.
     */
    public int eliminarFila() {
        DefaultTableModel modelo = (DefaultTableModel) this.getModel();
        if (modelo.getColumnCount() == 0) {
            Util.mensajeConfirmacion("La tabla no contiene elementos para eliminar");
        } else {
            int posicion = this.getSelectedRow();
            modelo.removeRow(posicion);
            return posicion;
        }
        return 0;
    }
}
