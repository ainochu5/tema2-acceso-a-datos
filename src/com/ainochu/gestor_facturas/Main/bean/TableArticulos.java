package com.ainochu.gestor_facturas.Main.bean;

import com.ainochu.gestor_facturas.Main.Class.Articulo;
import com.ainochu.gestor_facturas.Main.Class.Cliente;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;

/**
 * JTable que se encarga de rellenar la tabla de los articulos
 */
public class TableArticulos extends JTable {

    private DefaultTableModel modelo;

    public TableArticulos(){
        modelo = new DefaultTableModel();
    }

    public void anadirColumnas(){
        modelo.addColumn("Nombre");
        modelo.addColumn("Descripcion");
        modelo.addColumn("Precio");
        modelo.addColumn("Descuento");
        modelo.addColumn("Cantidad");
        this.setModel(modelo);
    }

    /**
     * Metodo que se encarga de rellenar la tabla con los array que recibe
     */
    public void rellenar(ArrayList<Articulo>listaArticulos){
        modelo.setNumRows(0);
        this.setModel(modelo);
        if(listaArticulos.size() == 0){
            return;
        }
        for(int i=0; i<listaArticulos.size(); i++){
            String datos [] = {listaArticulos.get(i).getNombreArticulo(),listaArticulos.get(i).getDescripcion(), Float.toString(listaArticulos.get(i).getPrecioArticulo()),
                    Boolean.toString(listaArticulos.get(i).isDescuentoArticulo()),Integer.toString(listaArticulos.get(i).getCantidadArticulos())};
            modelo.addRow(datos);
        }
        this.setModel(modelo);

    }

    public Articulo eliminarArticulo(ArrayList<Articulo> listaArticulos){
        //HACER QUE SE BORRE EL ELEMENTO DE LA LISTA
        Articulo ar = listaArticulos.get(this.getSelectedRow());
        modelo.removeRow(this.getSelectedRow());
        this.setModel(modelo);
        return ar;
    }

    public Articulo cargarArticulo(ArrayList<Articulo> listaArticulo){
        Articulo articulo = listaArticulo.get(this.getSelectedRow());
        return articulo;
    }


}
