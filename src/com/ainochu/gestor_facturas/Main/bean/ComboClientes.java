package com.ainochu.gestor_facturas.Main.bean;

import com.ainochu.gestor_facturas.Main.Class.Cliente;
import javafx.scene.control.ComboBox;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Combo que almacena los clientes del array ListaClientes
 */
public class ComboClientes extends JComboBox<String> {

    public ComboClientes(){

    }

    public void rellenarCombo(ArrayList<Cliente>listaClientes){
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        modelo.removeAllElements();
        if(listaClientes.size()==0){
            return;
        }
        for(int i=0; i<listaClientes.size(); i++){
            modelo.addElement(listaClientes.get(i).getNombre());
        }
        this.setModel(modelo);
    }
}
