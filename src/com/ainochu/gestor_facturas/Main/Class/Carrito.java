package com.ainochu.gestor_facturas.Main.Class;

/**
 * Created by Ainoa on 05/12/2015.
 */
public class Carrito {

    private int idFactura;
    private int idArticulo;
    private float total;

    public int getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(int idArticulo) {
        this.idArticulo = idArticulo;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
