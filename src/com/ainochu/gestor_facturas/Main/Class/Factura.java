package com.ainochu.gestor_facturas.Main.Class;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Atributos de la clase Factura
 */
public class Factura implements Serializable{

    //ATRIBUTOS
    private Date fechaFactura;
    private int idFactura;
    private String domicilioFactura;
    private String destinatarioFactura;
    private Float totalFactura;

    private ArrayList<Articulo> totalArticulos;


    public Factura(){
        totalArticulos = new ArrayList<>();
    }

    public String getDestinatarioFactura() {
        return destinatarioFactura;
    }

    public void setDestinatarioFactura(String destinatarioFactura) {
        this.destinatarioFactura = destinatarioFactura;
    }

    public String getDomicilioFactura() {
        return domicilioFactura;
    }

    public void setDomicilioFactura(String domicilioFactura) {
        this.domicilioFactura = domicilioFactura;
    }

    public Date getFechaFactura() {
        return fechaFactura;
    }

    public void setFechaFactura(Date fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    public int getIdFactura() {
        return idFactura;
    }

    public void setIdFactura(int idFactura) {
        this.idFactura = idFactura;
    }

    public Float getTotalFactura() {
        return totalFactura;
    }

    public void setTotalFactura(Float totalFactura) {
        this.totalFactura = totalFactura;
    }

    public ArrayList<Articulo> getTotalArticulos() {
        return totalArticulos;
    }

    public void setTotalArticulos(ArrayList<Articulo>totalArticulos) {
        this.totalArticulos = totalArticulos;
    }

    public String toString(){
        return destinatarioFactura + fechaFactura;
    }
}
