package com.ainochu.gestor_facturas.Main.Class;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Atributos de la clase Articulo
 */
public class Articulo implements Serializable {

    //DECLARADO PARA EVITAR ERRORES DE SERIALIZACIÓN.
    private static final long serialVersionUID = -6454823238829094097L;
    //ATRIBUTOS
    private int id_articulo;
    private String nombreArticulo;
    private String descripcion;
    private float precioArticulo;
    private int cantidadArticulos;
    private boolean descuentoArticulo;

    public int getId_articulo() {
        return id_articulo;
    }

    public void setId_articulo(int id_articulo) {
        this.id_articulo = id_articulo;
    }

    private ArrayList<Factura>listaArticulosFactura;

    public int getCantidadArticulos() {
        return cantidadArticulos;
    }

    public void setCantidadArticulos(int cantidadArticulos) {
        this.cantidadArticulos = cantidadArticulos;
    }

    public boolean isDescuentoArticulo() {
        return descuentoArticulo;
    }

    public void setDescuentoArticulo(boolean descuentoArticulo) {
        this.descuentoArticulo = descuentoArticulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombreArticulo() {
        return nombreArticulo;
    }

    public void setNombreArticulo(String nombreArticulo) {
        this.nombreArticulo = nombreArticulo;
    }

    public float getPrecioArticulo() {
        return precioArticulo;
    }

    public void setPrecioArticulo(float precioArticulo) {
        this.precioArticulo = precioArticulo;
    }

    public String toString(){
        return nombreArticulo + " " + descripcion;
    }
}
