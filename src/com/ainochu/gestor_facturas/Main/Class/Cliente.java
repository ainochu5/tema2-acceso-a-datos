package com.ainochu.gestor_facturas.Main.Class;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

/**
 * Atributos de la clase Cliente
 */
public class Cliente implements Serializable{

    //ATRIBUTOS
    private int id_cliente;
    private String nombre;
    private String apellidos;
    private String direccion;
    private Date fechaNacimiento;
    private String tipo_Cliente;
    private ArrayList<Factura>listaFacturasClientes;

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public Cliente(){
        listaFacturasClientes = new ArrayList<>();
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo_Cliente() {
        return tipo_Cliente;
    }

    public void setTipo_Cliente(String tipo_Cliente) {
        this.tipo_Cliente = tipo_Cliente;
    }

    public ArrayList<Factura> getListaFacturasClientes() {
        return listaFacturasClientes;
    }

    public void setListaFacturasClientes(ArrayList<Factura> listaFacturasClientes) {
        this.listaFacturasClientes = listaFacturasClientes;
    }
    @Override
    public String toString(){
      return nombre + " " + apellidos;
    }
}
