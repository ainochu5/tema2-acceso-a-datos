package com.ainochu.gestor_facturas.Main;
import com.ainochu.gestor_facturas.Main.Class.Articulo;
import com.ainochu.gestor_facturas.Main.Class.Carrito;
import com.ainochu.gestor_facturas.Main.Class.Cliente;
import com.ainochu.gestor_facturas.Main.Class.Factura;
import com.ainochu.gestor_facturas.Main.bean.*;
import com.ainochu.gestor_facturas.Main.database.BaseDatos;
import com.ainochu.gestor_facturas.Main.util.Excel;
import com.ainochu.gestor_facturas.Main.util.JFileCh;
import com.ainochu.gestor_facturas.Main.util.Util;
import com.ainochu.gestor_facturas.Main.util.XML;
import com.sun.org.apache.bcel.internal.generic.FADD;
import com.toedter.calendar.JDateChooser;
import static com.ainochu.gestor_facturas.Main.util.Constantes.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

/**
 * Aplicación que gestiona los datos de Clientes, Articulos y Genera facturas
 *
 * @author Ainoa Andrés
 * @version curso 2015-2016
 */
public class Main implements ActionListener{
    private static JFrame frame;
    private Connection conexion;
    private File archivoconf;
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    private JTextField tbNombre;
    private JTextField tbApellidos;
    private JButton btNuevoC;
    private JButton btGuardarC;
    private JDateChooser dateFecha;
    private JButton btNuevoA;
    private JTextField textNombreA;
    private JTextField textId_ProductoA;
    private JTextField textPrecioA;
    private JButton btGuardarA;
    private JButton btBusquedaA;
    private JTextField textDomicilioF;
    private ArrayList<Cliente> listaClientes;
    private ArrayList<Articulo> listaArticulos;
    private ArrayList<Factura> listaFacturas;
    private String[] tipo_cliente;
    private JButton btAnadirElemento;
    private JButton btNuevaF;
    private JButton btModificarF;
    private JButton btBusquedaF;
    private JButton btGuardarF;
    private JButton btModificarC;
    private JButton btBusquedaC;
    private JTextField textDomicilioC;
    private JPanel jpArticulos;
    private JPanel jpFacturas;
    private JPanel jpClientes;
    private JButton btModificarA;
    private ComboClientes cbTipoCliente;
    private JComboBox cbDescuentoA;
    private JTable tbArticulos;
    private ComboClientes cbNombresC;
    private ComboArticulos cbNombreA;
    private JScrollPane tbFFinal;
    private TableArticulos tableArticulos1;
    private JComboBox cbCantidadA;
    private JComboBox cbCantidadAF;
    private TableCarrito tbCarrito;
    private JList ltClientes;
    private JButton btEliminarC;
    private JList ltArticulos;
    private JList ltFacturas;
    private JButton btEliminarA;
    private JButton btEliminarF;
    private JButton btEliminarElemento;
    private JLabel lbTotalFactura;
    private JButton btExportarExcel;
    private JTextField textBusquedaNombre;
    private JComboBox cbBusquedaC;
    private JButton btBusquedaAC;
    private JLabel lbTipoCliente;
    private JLabel lbNombreC;
    private JPanel JPBusquedaC;
    private JPanel JPBusquedaA;
    private JButton btLimpiarB;
    private JLabel lbBNombreA;
    private JTextField textBNombreA;
    private JLabel lbBPrecio;
    private JButton btBusquedaAA;
    private JTextField textBPrecioA;
    private JButton btLimpiarA;
    private JTextField textBusquedaF;
    private JTextField textBusquedaFId;
    private JButton btBusquedaAF;
    private JButton btLimpiarF;
    private JPanel JPBusquedaF;
    private JDateChooser dateFechaFactura;
    private TableClientes tbClientes;
    private JButton btArticulosDescuento;
    private TableFacturas tbFacturas;
    private JButton btEliminarTipoCliente;
    private JButton btAplicarDto;
    private JLabel lbIdB;
    private JLabel lbDestinatarioB;
    private int cont = 0;
    private DefaultListModel lista;
    private ArrayList<Carrito>listaArticulosPorCliente;
    private File archivoClientes;
    private File archivoArticulo;
    private File archivoFactura;
    private int posicion;
    private boolean activado;
    private float total;
    private int contB =0;
    private BaseDatos baseDatos;
    private boolean aceptado;
    private String rol_usuario;
    private JMenu configuracion;
    private String destinatarioF;


    public Main(){
        //CARGAMOS EL ARCHIVO DE CONFIGURACION PARA COMPROBAR LAS RUTAS
        JFileCh jFileCh = new JFileCh();
        jFileCh.abrirArchivoConf();
        //CARGAMOS LA BASE DE DATOS
        baseDatos = new BaseDatos();
        try {
            conexion = baseDatos.conectar();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //CONECTAR PARA COMPROBAR SI EL USUARIO Y LA CONTRASEÑA SON CORRECTOS
        while (aceptado != true){
            aceptado = baseDatos.login(conexion);
        }
        Util.mensajeConfirmacion("Usuario correcto!");
        cargarVentana();
        if (baseDatos.getRol_usuario().equals("Admin")){
            configuracion.setEnabled(true);
        }
        else{
            configuracion.setEnabled(false);
        }
        //CARGO LAS COLUMNAS DE LAS TABLAS
        tbClientes.anadirColumnas();
        tableArticulos1.anadirColumnas();
        tbFacturas.anadirColumnas();
        //CARGAR BOTONES,TABLAS,MODELOS...
        cargarListeners();
        //LLAMAR AL METODO ENCARGADO DE REFRESCAR LA TABLA CLIENTES
        cargarRefrescoLista();
        //SABER EN QUE PANEL ESTOY
        tabbedPane1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                posicion = tabbedPane1.getSelectedIndex();
                super.mouseClicked(e);
            }
        });
        //CARGAR LAS TABLAS
        cargarTablas();
    }

    public void cargarVentana(){
        frame = new JFrame("GESTOR FACTURAS");
        //CrEAMOS EL JMENUBAR
        cambiarApariencia();
        frame.setJMenuBar(crearJMenuBar());
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setSize(750, 550);
        frame.setVisible(true);
    }


    public static void main(String[] args) {
        Main m = new Main();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String opcion = e.getActionCommand();
        System.out.println(posicion);
        Cliente c1 = null;
        Factura f1 = null;
        Articulo a1;
        BaseDatos baseDatos = new BaseDatos();
        switch (opcion){
            //CASO NUEVO CLIENTE/ARTICULO/FACTURA
            case "Nuevo":
                if(posicion == 0) {
                    limpiarCajas(posicion);
                    cargarTipoCliente();
                    estadoCajas(true,0);
                    activado = false;
                }
                if(posicion == 1){
                    limpiarCajas(posicion);
                    rellenarcbCantidad();
                    rellenarcbOferta();
                    activado = false;
                    estadoCajas(true,1);
                }
                if(posicion == 2){
                    listaClientes = baseDatos.obtenerNombreCliente(conexion);
                    cbNombresC.rellenarCombo(listaClientes);
                    limpiarCajas(2);
                    listaArticulos = baseDatos.obtenerNombreArticulo(conexion);
                    cbNombreA.rellenarComboNombre(listaArticulos);
                    tbCarrito.formatearTabla();
                    lbTotalFactura.setText("0.0");
                    rellenarcbCantidad();
                    activado = false;
                    estadoCajas(true,2);
                    textDomicilioF.setEnabled(false);
                }
                break;
            case "Guardar":
                //CASO NUEVO CLIENTE/ARTICULO/FACTURA
                if(posicion == 0) {
                    c1 = new Cliente();
                    //GUARDAR AL CLIENTE EN LA BD
                    boolean resultado = controlarErroresCliente();
                    c1.setNombre(tbNombre.getText());
                    c1.setApellidos(tbApellidos.getText());
                    c1.setDireccion(textDomicilioC.getText());
                    c1.setFechaNacimiento(dateFecha.getDate());
                    c1.setTipo_Cliente(cbTipoCliente.getSelectedItem().toString());
                    if (resultado == false){
                        return;
                    }
                    baseDatos.agregarNuevoCliente(c1, conexion);
                    //AÑADIR DATOS A LA TABLA
                    listaClientes = baseDatos.refrescarTablaClientes(conexion);
                    //PASAR LOS DATOS A LA TABLA
                    tbClientes.rellenarTabla(listaClientes);
                    btGuardarC.setEnabled(false);

                }
                if(posicion == 1) {
                    //GUARDAR AL ARTICULO EN LA BD
                    a1 = new Articulo();
                    boolean resultado = controlarErroresArticulo();
                    a1.setNombreArticulo(textNombreA.getText());
                    a1.setDescripcion(textId_ProductoA.getText());
                    a1.setCantidadArticulos(Integer.parseInt(cbCantidadA.getSelectedItem().toString()));
                    a1.setDescuentoArticulo(Boolean.parseBoolean(cbDescuentoA.getSelectedItem().toString()));
                    if(textPrecioA.getText().isEmpty()){
                        a1.setPrecioArticulo(0);
                        Util.mensajeConfirmacion("Se asignara un 0 por defecto");
                        textPrecioA.setText("0");
                    }
                    else{
                        a1.setPrecioArticulo(Float.parseFloat(textPrecioA.getText()));
                    }
                    if (resultado == false)
                        return;
                    baseDatos.agregarNuevoArticulo(a1, conexion);
                    listaArticulos = baseDatos.refrescarTablaProductos(conexion);
                    //PASAR LOS DATOS A LA TABLA
                    tableArticulos1.rellenar(listaArticulos);
                    btGuardarA.setEnabled(false);
                    //CARGAR LA TABLA Y LA RELLENO CON EL NUEVO ARTICULO
                    tableArticulos1.rellenar(listaArticulos);
                }
                if(posicion == 2) {
                    //GUARDAR LA FACTURA EN LA BD
                    f1 = new Factura();
                    int pos = cbNombresC.getSelectedIndex();
                    f1.setDestinatarioFactura(cbNombresC.getSelectedItem().toString());
                    if (textDomicilioF.equals("")) {
                        f1.setDomicilioFactura(destinatarioF);
                    } else {
                        f1.setDomicilioFactura(textDomicilioF.getText());
                    }
                    f1.setFechaFactura(dateFechaFactura.getDate());
                    baseDatos.agregarNuevaFactura(f1, conexion, listaArticulosPorCliente);
                    btGuardarF.setEnabled(false);
                     //ACTUALIZAR LA TABLA PARA MOSTRAR SUS ELEMENTOS
                    listaFacturas = baseDatos.refrescarTablaFacturas(conexion);
                    tbFacturas.rellenarTabla(listaFacturas);

                }
                break;
            case "Anadir":
                //METODO QUE SE ENCARGA DE AÑADIR AL ARRAY LISTAARTICULOSPORCLIENTE CADA ARTICULO A LA FACTURA
                anadir();
                break;
            case "Modificar":
                rellenarcbCantidad();
                rellenarcbOferta();
                //JUGAR CON LAS POSICIONES Y EN FUNCION DE ELLAS, RELLENAR CADA PANEL CON LOS DATOS
                if (posicion == 0) {
                    Cliente cliente = tbClientes.cargarCliente(listaClientes);
                    String nombreAntiguo = cliente.getNombre();
                    boolean resultado = controlarErroresCliente();
                    cliente.setNombre(tbNombre.getText());
                    cliente.setApellidos(tbApellidos.getText());
                    cliente.setDireccion(textDomicilioC.getText());
                    cliente.setFechaNacimiento(dateFecha.getDate());
                    cliente.setTipo_Cliente(cbTipoCliente.getSelectedItem().toString());
                    if (resultado == false){
                        return;
                    }
                    while(nombreAntiguo.equals(tbNombre)){
                        System.out.println("Los nombres no pueden ser iguales");
                    }
                    baseDatos.modificarCliente(cliente, conexion, nombreAntiguo);
                    estadoCajas(true,0);
                    //ACTUALIZAR LA TABLA
                    listaClientes = baseDatos.refrescarTablaClientes(conexion);
                    tbClientes.rellenarTabla(listaClientes);
                }
                if (posicion == 1){
                    Articulo articulo = tableArticulos1.cargarArticulo(listaArticulos);
                    String articuloAntiguo = articulo.getNombreArticulo();
                    boolean resultado = controlarErroresArticulo();
                    articulo.setNombreArticulo(textNombreA.getText());
                    articulo.setDescripcion(textId_ProductoA.getText());
                    articulo.setCantidadArticulos(Integer.parseInt(cbCantidadA.getSelectedItem().toString()));
                    articulo.setDescuentoArticulo(Boolean.parseBoolean(cbDescuentoA.getSelectedItem().toString()));
                    if(textPrecioA.getText().isEmpty()){
                        articulo.setPrecioArticulo(0);
                        Util.mensajeConfirmacion("Se asignara un 0 por defecto");
                        textPrecioA.setText("0");
                    }
                    else{
                        articulo.setPrecioArticulo(Float.parseFloat(textPrecioA.getText()));
                    }
                    if (resultado == false)
                        return;
                    while(articuloAntiguo.equals(textNombreA)){
                        System.out.println("El nombre del articulo no puede ser igual");
                    }
                    baseDatos.modificarArticulo(articulo, conexion, articuloAntiguo);
                    estadoCajas(true, 1);
                    //ACTUALIZAR LA TABLA
                    listaArticulos=baseDatos.refrescarTablaProductos(conexion);
                    tableArticulos1.rellenar(listaArticulos);
                }
                if (posicion == 2){
                    textDomicilioF.setEnabled(true);
                    Factura factura = tbFacturas.cargarFactura(listaFacturas);
                    int idFactura = factura.getIdFactura();
                    boolean resultado = controlarErroresFactura();
                    //PILLAR AL DESTINATARIO
                    factura.setDestinatarioFactura(cbNombresC.getSelectedItem().toString());
                    if (textDomicilioF.equals("")) {
                        factura.setDomicilioFactura(destinatarioF);
                    } else {
                        factura.setDomicilioFactura(textDomicilioF.getText());
                    }
                    //ASIGNO UN ID A LA FACTURA A TRAVÉS DEL CONTADOR
                    factura.setFechaFactura(dateFechaFactura.getDate());
                    if(resultado == false){
                        return;
                    }
                    baseDatos.modificarFactura(factura,conexion,idFactura);
                    estadoCajas(true, 2);
                    listaFacturas = baseDatos.refrescarTablaFacturas(conexion);
                    tbFacturas.rellenarTabla(listaFacturas);
                }
                break;
            case "Eliminar":
                if (posicion == 0) {
                    c1 = tbClientes.eliminarCliente(listaClientes);
                    baseDatos.eliminarCliente(c1, conexion);
                    Util.mensajeConfirmacion("Cliente eliminado");
                    limpiarCajas(posicion);
                }
                if (posicion == 1){
                    Articulo articulo = tableArticulos1.eliminarArticulo(listaArticulos);
                    baseDatos.eliminarProducto(articulo, conexion);
                    Util.mensajeConfirmacion("Articulo eliminado");
                    limpiarCajas(posicion);
                }
                if (posicion == 2){
                    f1 = tbFacturas.eliminarFactura(listaFacturas);
                    baseDatos.eliminarFactura(f1, conexion);
                    Util.mensajeConfirmacion("Factura eliminada");
                    limpiarCajas(posicion);
                }
                break;
            case "Eliminar elemento":
                //EN CASO DE QUE LA TABLA NO TENGA ELEMENTOS, MOSTRARA EL MENSAJE POR PANTALLA
                if (tbCarrito.getColumnCount()==0){
                    Util.mensajeConfirmacion("La tabla no contiene elementos para eliminar");
                    return;
                }
                if(tbCarrito.getSelectedRow()==-1){
                    Util.mensajeConfirmacion("Seleccione un elemento de la tabla");
                }
                //EN OTRO CASO SE BORRARA EL ELEMENTO DE LA TABLA Y SE RECALCULARA EL TOTAL DE LA FACTURA
                else {
                    Factura factura = tbFacturas.cargarFactura(listaFacturas);
                    Articulo articulo = listaArticulos.get(tbCarrito.getSelectedRow());
                    //int idArticulo = baseDatos.funcionDevolverIdProducto("get_id_producto",conexion,factura.getIdFactura(),articulo.getPrecioArticulo());
                    baseDatos.modificarTablaCarrito(conexion,factura.getIdFactura(),articulo.getId_articulo());
                    listaArticulos = baseDatos.refrescarTablaCarrito(factura.getIdFactura(),conexion);
                    tbCarrito.rellenarTabla(listaArticulos);
                    total = baseDatos.totalCarrito("get_total_carrito", conexion, factura.getIdFactura());
                    lbTotalFactura.setText(Float.toString(total));
                }
                break;
            case "Exportar xml":
                //LLAMAMOS A LA CLASE XML PARA EXPORTAR LOS DATOS A UNA UBICACIÓN FIJA. PASAMOS EL ARRAYLIST DE ARTICULOS
                XML xml = new XML();
                listaArticulos = baseDatos.refrescarTablaProductos(conexion);
                listaClientes = baseDatos.refrescarTablaClientes(conexion);
                listaFacturas = baseDatos.refrescarTablaFacturas(conexion);
                listaArticulosPorCliente = baseDatos.devolverDatosCarrito(conexion);
                xml.escribirXMLArticulo(listaArticulos);
                xml.escribirXMLClientes(listaClientes);
                xml.escribirXMLFacturas(listaFacturas);
                xml.escribirXMLCarrito(listaArticulosPorCliente);
                Util.mensajeConfirmacion("Exportacion correcta");
                break;
            case "Importar xml":
                //LLAMAMOS A LA CLASE XML PARA IMPORTAR LOS DATOS DESDE LA UBICACION QUE SELEECIONE EL USUARIO.
                xml = new XML();
                listaArticulos = xml.leerFicheroArticulosXML(listaArticulos);
                listaClientes = xml.leerFicheroClientesXML(listaClientes);
                listaFacturas = xml.leerFicheroFacturasXML(listaFacturas);
                listaArticulosPorCliente = xml.leerFicheroCarritoXML(listaArticulosPorCliente);
                //PASAR LOS DATOS A LA BASE DE DATOS
                baseDatos.agregarClientes(conexion, listaClientes);
                baseDatos.agregarArticulos(conexion, listaArticulos);
                baseDatos.agregarCarritos(conexion, listaFacturas, listaArticulosPorCliente);
                Util.mensajeConfirmacion("Importacion correcta");
                tableArticulos1.rellenar(listaArticulos);
                tbClientes.rellenarTabla(listaClientes);
                tbFacturas.rellenarTabla(listaFacturas);
                break;
            //DEPENDIENDO DE SI ESTA PULSADO EL BOTON O NO, SE CAMBIA EL TAMAÑO DE LA VENTANA
            case "Busqueda":
                if (posicion == 0) {
                    if (contB % 2 == 0) {
                        frame.setSize(1000,550);
                        JPBusquedaC.setVisible(true);
                        cargarBusquedaCliente();
                        cbBusquedaC.setSelectedIndex(0);
                    } else {
                        JPBusquedaC.setVisible(false);
                        frame.setSize(750, 550);
                    }
                    contB++;
                }
                if (posicion == 1){
                    if (contB % 2 == 0) {
                        frame.setSize(1000,550);
                        JPBusquedaA.setVisible(true);
                    } else {
                        JPBusquedaA.setVisible(false);
                        frame.setSize(750, 550);
                    }
                    contB++;
                }
                if (posicion == 2){
                    if (contB % 2 == 0) {
                        frame.setSize(1000,550);
                        JPBusquedaF.setVisible(true);
                    } else {
                        JPBusquedaF.setVisible(false);
                        frame.setSize(750,550);
                    }
                    contB++;
                }
                break;
            //REALIZAR LA BUSQUEDA POR NOMBRE Y POR TIPO DE CLIENTE
            case "RBC":
                String nombreCliente;
                if(textBusquedaNombre.getText().isEmpty()){
                    nombreCliente = null;
                }
                else{
                    nombreCliente = textBusquedaNombre.getText();
                }
                String tipoCliente = cbBusquedaC.getSelectedItem().toString();
                listaClientes = baseDatos.busquedaCliente(conexion,nombreCliente,tipoCliente);
                tbClientes.rellenarTabla(listaClientes);
                break;
            //REALIZA LA BUSQUEDA POR NOMBRE Y POR PRECIO DE PRODUCTO
            case "RBA":
                String nombreArticulo;
                float precioArticulo;
                if(textBNombreA.getText().isEmpty() && (!textBPrecioA.getText().isEmpty())){
                    nombreArticulo = "";
                    precioArticulo = Float.parseFloat(textBPrecioA.getText());
                }
                else {
                    nombreArticulo = textBNombreA.getText();
                    precioArticulo = 0;
                }
                listaArticulos = baseDatos.busquedaArticulo(conexion,nombreArticulo,precioArticulo);
                tableArticulos1.rellenar(listaArticulos);
                break;
            case "RBF":
                String destinatarioFactura;
                int idFactura;
                if(textBusquedaF.getText().isEmpty() && (!textBusquedaFId.getText().isEmpty())){
                    destinatarioFactura = "";
                    idFactura = Integer.parseInt(textBusquedaFId.getText());
                }
                else {
                    destinatarioFactura = textBusquedaF.getText();
                    idFactura = 0;
                }
                listaFacturas = baseDatos.busquedaFactura(conexion,destinatarioFactura,idFactura);
                tbFacturas.rellenarTabla(listaFacturas);
                break;
            case "LimpiarC":
                textBusquedaNombre.setText("");
                cbBusquedaC.setSelectedItem("");
                listaClientes = baseDatos.refrescarTablaClientes(conexion);
                tbClientes.rellenarTabla(listaClientes);
                contB = 0;
                break;
            case "LimpiarA":
                textBNombreA.setText("");
                textBPrecioA.setText("");
                contB = 0;
                listaArticulos = baseDatos.refrescarTablaProductos(conexion);
                tableArticulos1.rellenar(listaArticulos);
                break;
            case "LimpiarF":
                textBusquedaF.setText("");
                textBusquedaFId.setText("");
                listaFacturas = baseDatos.refrescarTablaFacturas(conexion);
                tbFacturas.rellenarTabla(listaFacturas);
                contB = 0;
                break;
            //ABRE LA PESTAÑA CONFIGURACION DONDE EL USUARIO PODRA ELEGIR LA BD, LA RUTA Y EL NOMBRE
            case "Configuracion":
                Config config = new Config();
                System.out.print("OKEY");
                break;
            case "Descuento":
               int valor = baseDatos.funcionCantidadArticulosDescuento("get_cantidad_productos_descuento",conexion);
                Util.mensajeConfirmacion("La cantidad de artículos con descuento son: " + valor);
                break;
            case "Clientes":
                //PILLO EL VALOR DEL COMBOBOX Y LE PASO EL PARAMETRO A LA FUNCION
                String nombreC = cbNombresC.getSelectedItem().toString();
                destinatarioF = baseDatos.funcionNombreDomicilioFactura("get_domicilio_factura",conexion,nombreC);
                textDomicilioF.setText(destinatarioF);
                break;
            case "Eliminar tipo":
                //PILLO EL VALOR DEL COMBO Y SE LO PASO AL PROCEDIMIENTO
                tipoCliente = cbBusquedaC.getSelectedItem().toString();
                baseDatos.procedimientoEliminarTipoCliente("eliminar_tipo_cliente",conexion,tipoCliente);
                listaClientes = baseDatos.refrescarTablaClientes(conexion);
                tbClientes.rellenarTabla(listaClientes);
                break;
            case "Aplicar dto":
                baseDatos.procedimientoAplicarDescuentoArticulos("aplicar_descuento_productos",conexion);
                listaArticulos = baseDatos.refrescarTablaProductos(conexion);
                tableArticulos1.rellenar(listaArticulos);
                break;
            default:
                break;
        }
    }

    //INDICA LA POSICION DE LAS CAJAS SEGUN EL ESTADO QUE SE LE PASE POR PARAMETRO Y LA POSICION EN LA QUE ESTAS
    private void estadoCajas(boolean estado, int posicion){
        if (posicion == 0) {
            tbNombre.setEnabled(estado);
            tbApellidos.setEnabled(estado);
            textDomicilioC.setEnabled(estado);
            cbTipoCliente.setEnabled(estado);
            dateFecha.setEnabled(estado);
            btGuardarC.setEnabled(estado);
        }
        if (posicion == 1) {
            textNombreA.setEnabled(estado);
            textId_ProductoA.setEnabled(estado);
            textPrecioA.setEnabled(estado);
            cbDescuentoA.setEnabled(estado);
            cbCantidadA.setEnabled(estado);
            btGuardarA.setEnabled(estado);
        }
        if (posicion == 2) {
            textDomicilioF.setEnabled(estado);
            cbNombresC.setEnabled(estado);
            cbCantidadAF.setEnabled(estado);
            dateFechaFactura.setEnabled(estado);
            cbNombreA.setEnabled(estado);
            btGuardarF.setEnabled(estado);
            btAnadirElemento.setEnabled(estado);
            btEliminarElemento.setEnabled(estado);
        }
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    private void rellenarcbCantidad(){
        for (int i=1; i<=10; i++){
            cbCantidadA.addItem(i);
            cbCantidadAF.addItem(i);
        }
    }
    private void rellenarcbOferta(){
        cbDescuentoA.addItem("true");
        cbDescuentoA.addItem("false");
    }


    //CARGAR AL CLIENTE QUE SE SELECCIONE EN LA LISTA
    private void cargarCliente(Cliente cliente){
        tbNombre.setText(cliente.getNombre());
        tbApellidos.setText(cliente.getApellidos());
        textDomicilioC.setText(cliente.getDireccion());
        cbTipoCliente.setSelectedItem(cliente.getTipo_Cliente());
        dateFecha.setDate(cliente.getFechaNacimiento());

    }
    //CARGAR AL ARTICULO SELECCIONADO EN LA LISTA
    private void cargarArticulo(Articulo articulo) {
        textNombreA.setText(articulo.getNombreArticulo());
        textId_ProductoA.setText(articulo.getDescripcion());
        textPrecioA.setText(Float.toString(articulo.getPrecioArticulo()));
        cbDescuentoA.setSelectedItem(articulo.isDescuentoArticulo());
        cbCantidadA.setSelectedItem(articulo.getCantidadArticulos());
    }
    //CARGAR LA FACTURA SELECCIONADA EN LA LISTA
    private void cargarFactura(Factura factura){
        listaClientes = baseDatos.obtenerNombreCliente(conexion);
        cbNombresC.rellenarCombo(listaClientes);
        total = 0;
        lbTotalFactura.setText("0.0");
        textDomicilioF.setText(factura.getDomicilioFactura());
        System.out.println(factura.getDestinatarioFactura());
        cbNombresC.setSelectedItem(factura.getDestinatarioFactura());
        cbCantidadAF.setSelectedItem(null);
        cbNombreA.setSelectedItem(null);
        lbTotalFactura.setText(Float.toString(total));
        cbNombreA.setSelectedItem(0);
        cbCantidadAF.setSelectedItem(0);
        dateFechaFactura.setDate(factura.getFechaFactura());

    }
    //EN CASO DE NUEVO, LIMPIA LAS CAJAS
    private void limpiarCajas(int posicion){
        if (posicion == 0) {
            tbNombre.setText("");
            tbApellidos.setText("");
            textDomicilioC.setText("");
            cbTipoCliente.removeAllItems();
            dateFecha.setDate(null);
        }
        if (posicion == 1){
            textNombreA.setText("");
            textId_ProductoA.setText("");
            textPrecioA.setText("");
            cbDescuentoA.removeAllItems();
            cbCantidadA.removeAllItems();
        }
        if (posicion == 2){
            textDomicilioF.setText("");
            cbCantidadAF.setSelectedItem(null);
            cbNombreA.setSelectedItem(null);
        }
    }

    //CARGAR EL COMBOBOX DEL TIPO DE CLIENTE
    private void cargarTipoCliente(){
        tipo_cliente = new String[]{"Introduzca una opcion","Autonomo", "Empresario", "Empleado"};
        for (int i = 0; i < tipo_cliente.length; i++) {
            cbTipoCliente.addItem(tipo_cliente[i]);
        }
    }

    /**
     * Metodo que se encarga de incluir un nuevo articulo a la factura, la cual mostrará dicho elemento
     * a través del metodo rellenartabla, en la que le paso como parámetro el array listaarticuloscliente.
     *
     */
    private void anadir(){
        if (cbCantidadAF.getSelectedIndex()==-1 && cbNombreA.getSelectedIndex()==-1){
            Util.mensajeConfirmacion("Seleccione nombre de articulo y cantidad para añadir");
            return;
        }
       //PILLO EL NOMBRE DEL ARTICULO Y SU CANTIDAD
        String nombreBuscar = cbNombreA.getSelectedItem().toString();
        int cantidadA = Integer.parseInt(cbCantidadAF.getSelectedItem().toString());
        //LLAMO AL MÉTODO QUE SE ENCARGA DE PROCESAR LOS DATOS
        Articulo articulo = baseDatos.devolverDatosProducto(nombreBuscar,cantidadA,conexion);
        Carrito carrito = new Carrito();
        carrito.setIdArticulo(articulo.getId_articulo());
        carrito.setTotal(articulo.getPrecioArticulo());
        listaArticulosPorCliente.add(carrito);
    }

    //CREA EL JMENUBAR DE NUESTRO FRAME
    private JMenuBar crearJMenuBar(){

        JMenu archivo = new JMenu("Archivo");
        configuracion = new JMenu("Configuracion");
        JMenuItem importar = new JMenuItem("Importar...");
        JMenuItem exportar = new JMenuItem("Exportar...");
        JMenuItem configurar = new JMenuItem("Configurar");
        configurar.addActionListener(this);

        configurar.setActionCommand("Configuracion");
        exportar.addActionListener(this);
        exportar.setActionCommand("Exportar xml");
        importar.addActionListener(this);
        importar.setActionCommand("Importar xml");

        archivo.add(importar);
        archivo.add(exportar);
        configuracion.add(configurar);
        JMenuBar mb = new JMenuBar();
        mb.add(archivo);
        mb.add(configuracion);

      return mb;
    }

    //SE UTILIZA PARA HACER QUE EL SISTEMA NO PILLE POR DEFECTO LA APARIENCIA DEL SO.
    private void cambiarApariencia(){
        try{
            for (javax.swing.UIManager.LookAndFeelInfo info: javax.swing.UIManager.getInstalledLookAndFeels()){
                if ("Nimbus".equals(info.getName())){
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        }catch (Exception e){
            System.out.print(e);
        }
    }

    //AQUI CONTROLAMOS TODOS LOS POSIBLES ERRORES COMO CAJAS VACIAS Y CB SIN SELECCIONAR.
    private boolean controlarErroresCliente(){
        if(cbTipoCliente.getSelectedIndex()==0){
            Util.mensajeConfirmacion("Seleccione tipo de cliente");
            cbTipoCliente.setBackground(Color.red);
            return false;
        }
        else{
            cbTipoCliente.setBackground(Color.white);
        }
        if(tbNombre.getText().isEmpty()){
            Util.mensajeConfirmacion("El nombre de cliente no puede estar vacio");
            tbNombre.setBackground(Color.red);
            return false;
        }
        else{
            tbNombre.setBackground(Color.white);
        }
        return true;
    }

    private boolean controlarErroresArticulo(){
        if (textNombreA.getText().isEmpty()){
            Util.mensajeConfirmacion("El nombre del articulo no puede estar vacio");
            textNombreA.setBackground(Color.red);
            return false;
        }
        else{
            textNombreA.setBackground(Color.white);
        }

        if (textId_ProductoA.getText().isEmpty()){
            Util.mensajeConfirmacion("El id del producto no puede estar vacio");
            textId_ProductoA.setBackground(Color.red);
            return false;
        }
        else{
            textId_ProductoA.setBackground(Color.white);
        }
        if (textPrecioA.getText().isEmpty()){
            Util.mensajeConfirmacion("El precio no puede estar vacio");
            textPrecioA.setBackground(Color.red);
            return false;
        }
        else{
            textPrecioA.setBackground(Color.white);
        }
        return true;
    }

    private boolean controlarErroresFactura() {
        if (textDomicilioF.getText().isEmpty()) {
            Util.mensajeConfirmacion("La direccion no puede estar vacia");
            textDomicilioF.setBackground(Color.red);
            return false;
        } else {
            textDomicilioF.setBackground(Color.white);
        }
        return true;
    }

    private void cargarBusquedaCliente(){
        for (int i = 0; i < tipo_cliente.length; i++){
            cbBusquedaC.addItem(tipo_cliente[i]);
        }
    }

    private void cargarListeners(){
        tableArticulos1.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                btEliminarA.setEnabled(true);
                btModificarA.setEnabled(true);
                Articulo articulo = tableArticulos1.cargarArticulo(listaArticulos);
                cargarArticulo(articulo);
                super.mouseClicked(e);
            }
        });
        tbClientes.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                btEliminarC.setEnabled(true);
                btModificarC.setEnabled(true);
                Cliente cliente = tbClientes.cargarCliente(listaClientes);
                cargarCliente(cliente);
                super.mouseClicked(e);
            }
        });
        tbFacturas.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                estadoCajas(true,2);
                btEliminarF.setEnabled(true);
                btModificarF.setEnabled(true);
                Factura factura = tbFacturas.cargarFactura(listaFacturas);
                cargarFactura(factura);
                int idFactura = factura.getIdFactura();
                listaArticulos = baseDatos.refrescarTablaCarrito(idFactura, conexion);
                tbCarrito.rellenarTabla(listaArticulos);
                listaArticulosPorCliente.clear();
                total = baseDatos.totalCarrito("get_total_carrito", conexion, idFactura);
                lbTotalFactura.setText(Float.toString(total));
            }
        });
        tbCarrito.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                btAnadirElemento.setEnabled(true);
                btEliminarElemento.setEnabled(true);
                super.mouseClicked(e);
            }
        });
        //CREAMOS LOS LISTENERS
        btModificarF.setEnabled(false);
        btEliminarF.setEnabled(false);
        btModificarA.setEnabled(false);
        btEliminarA.setEnabled(false);
        btModificarC.setEnabled(false);
        btEliminarC.setEnabled(false);
        btNuevoC.addActionListener(this);
        btNuevoC.setActionCommand("Nuevo");
        btNuevoA.addActionListener(this);
        btNuevoA.setActionCommand("Nuevo");
        btNuevaF.addActionListener(this);
        btNuevaF.setActionCommand("Nuevo");
        btGuardarC.addActionListener(this);
        btGuardarC.setActionCommand("Guardar");
        btGuardarA.addActionListener(this);
        btGuardarA.setActionCommand("Guardar");
        btGuardarF.addActionListener(this);
        btGuardarF.setActionCommand("Guardar");
        btAnadirElemento.addActionListener(this);
        btAnadirElemento.setActionCommand("Anadir");
        btModificarC.addActionListener(this);
        btModificarC.setActionCommand("Modificar");
        btEliminarC.addActionListener(this);
        btEliminarC.setActionCommand("Eliminar");
        btModificarA.addActionListener(this);
        btModificarA.setActionCommand("Modificar");
        btEliminarA.addActionListener(this);
        btEliminarA.setActionCommand("Eliminar");
        btModificarF.addActionListener(this);
        btModificarF.setActionCommand("Modificar");
        btEliminarF.addActionListener(this);
        btEliminarF.setActionCommand("Eliminar");
        btEliminarElemento.addActionListener(this);
        btEliminarElemento.setActionCommand("Eliminar elemento");
        btBusquedaC.addActionListener(this);
        btBusquedaC.setActionCommand("Busqueda");
        btBusquedaA.addActionListener(this);
        btBusquedaA.setActionCommand("Busqueda");
        btBusquedaF.setActionCommand("Busqueda");
        btBusquedaF.addActionListener(this);
        listaArticulosPorCliente = new ArrayList<>();
        btBusquedaAC.addActionListener(this);
        btBusquedaAC.setActionCommand("RBC");
        btBusquedaAA.addActionListener(this);
        btBusquedaAA.setActionCommand("RBA");
        btBusquedaAF.addActionListener(this);
        btBusquedaAF.setActionCommand("RBF");
        btLimpiarB.addActionListener(this);
        btLimpiarB.setActionCommand("LimpiarC");
        btLimpiarA.addActionListener(this);
        btLimpiarA.setActionCommand("LimpiarA");
        btLimpiarF.addActionListener(this);
        btLimpiarF.setActionCommand("LimpiarF");
        btArticulosDescuento.setActionCommand("Descuento");
        btArticulosDescuento.addActionListener(this);
        cbNombresC.setActionCommand("Clientes");
        cbNombresC.addActionListener(this);
        btEliminarTipoCliente.setActionCommand("Eliminar tipo");
        btEliminarTipoCliente.addActionListener(this);
        btAplicarDto.setActionCommand("Aplicar dto");
        btAplicarDto.addActionListener(this);
        activado = false;
        //RELLENAR COMBOS
        cargarTipoCliente();
        rellenarcbCantidad();
        rellenarcbOferta();
        rellenarcbCantidad();
        //MANTENER LOS PANELES DE BUSQUEDA DESACTIVADOS
        JPBusquedaC.setVisible(false);
        JPBusquedaA.setVisible(false);
        JPBusquedaF.setVisible(false);
        estadoCajas(false,0);
        estadoCajas(false,1);
        estadoCajas(false, 2);
    }
    public void cargarRefrescoLista(){
        Timmer modelo = new Timmer();
        modelo.addObserver (new Observer()
        {
            public void update (Observable unObservable, Object dato)
            {
                listaClientes = baseDatos.refrescarTablaClientes(conexion);
                tbClientes.rellenarTabla(listaClientes);
                System.out.println("Refrescado");
            }
        });
    }

    public void cargarTablas(){
        listaClientes = baseDatos.refrescarTablaClientes(conexion);
        listaArticulos = baseDatos.refrescarTablaProductos(conexion);
        listaFacturas = baseDatos.refrescarTablaFacturas(conexion);
        tbClientes.rellenarTabla(listaClientes);
        tableArticulos1.rellenar(listaArticulos);
        tbFacturas.rellenarTabla(listaFacturas);
    }

}
