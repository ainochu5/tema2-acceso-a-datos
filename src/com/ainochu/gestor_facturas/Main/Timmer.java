package com.ainochu.gestor_facturas.Main;

import com.ainochu.gestor_facturas.Main.bean.TableClientes;

import java.util.Date;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

import com.ainochu.gestor_facturas.Main.database.BaseDatos;
import com.ainochu.gestor_facturas.Main.database.BaseDatos.*;
import com.ainochu.gestor_facturas.Main.bean.TableClientes.*;

/**
 * Created by Ainoa on 06/12/2015.
 */
public class Timmer extends Observable {
    private TimerTask timerTask;
    private TableClientes tbClientes;

    /**
         * Lanza un timer cada segundo.
         */
        public Timmer()
        {
            timerTask = new TimerTask()
            {
                public void run()
                {
                    setChanged();
                    notifyObservers(new Date());
                }
            };
            Timer timer = new Timer();
            timer.scheduleAtFixedRate(timerTask,0, 30000);
        }


}
