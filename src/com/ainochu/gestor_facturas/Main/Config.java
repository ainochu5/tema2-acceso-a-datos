package com.ainochu.gestor_facturas.Main;

import com.ainochu.gestor_facturas.Main.util.JFileCh;
import com.ainochu.gestor_facturas.Main.util.Util;

import static com.ainochu.gestor_facturas.Main.util.Constantes.NOMBREBD;
import static com.ainochu.gestor_facturas.Main.util.Constantes.RUTABD;
import static com.ainochu.gestor_facturas.Main.util.Constantes.TIPOCONEXION;

import javax.swing.*;
import java.awt.event.*;

/**
 * Dialog dise�ado para guardar la nueva ruta del archivo de configuracion.
 */
public class Config extends JDialog implements ActionListener {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JLabel lbruta;
    private JTextArea textRuta;
    private JTextField textNombreBD;
    private JComboBox cbConexion;
    private String tipoConexion;

    public String getTipoConexion() {
        return tipoConexion;
    }

    public void setTipoConexion(String tipoConexion) {
        this.tipoConexion = tipoConexion;
    }

    public Config() {
        setContentPane(contentPane);
        setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
        pack();
        setModal(true);
        setLocationRelativeTo(null);
        rellenarCombo();
        buttonOK.addActionListener(this);
        buttonOK.setActionCommand("Aceptar");
        buttonCancel.addActionListener(this);
        buttonCancel.setActionCommand("Cancelar");
        setVisible(true);
    }
    /**
     *
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Aceptar":
                 TIPOCONEXION = cbConexion.getSelectedItem().toString();
                if(textRuta.getText().isEmpty()|| textNombreBD.getText().isEmpty()){
                    return;
                }
                else{
                    RUTABD = textRuta.getText();
                    NOMBREBD = textNombreBD.getText();
                    JFileCh jFileCh = new JFileCh();
                    jFileCh.escribirFichero();
                }
                Util.mensajeConfirmacion("Los cambios se guardaron correctamente");
                setVisible(false);
                break;
            default:
                Util.mensajeConfirmacion("No se han efectuado cambios");
                setVisible(false);
                break;
        }
    }

    public void rellenarCombo(){
        cbConexion.addItem("mysql");
        cbConexion.addItem("postgresql");
    }
}
