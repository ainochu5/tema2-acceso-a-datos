package com.ainochu.gestor_facturas.Main.database;

import com.ainochu.gestor_facturas.Main.Class.Articulo;
import com.ainochu.gestor_facturas.Main.Class.Carrito;
import com.ainochu.gestor_facturas.Main.Class.Cliente;
import com.ainochu.gestor_facturas.Main.Class.Factura;
import com.ainochu.gestor_facturas.Main.Login;
import com.ainochu.gestor_facturas.Main.bean.ComboArticulos;
import static com.ainochu.gestor_facturas.Main.util.Constantes.*;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.zip.CheckedOutputStream;

/**
 * Created by Ainoa on 01/12/2015.
 */
public class BaseDatos {


    public Connection conexion;
    private String rol_usuario;

    public String getRol_usuario() {
        return rol_usuario;
    }

    public void setRol_usuario(String rol_usuario) {
        this.rol_usuario = rol_usuario;
    }

    //TERMINAR LOGIN DEL CLIENTE
    public boolean login(Connection conexion) {
        Login login = new Login();
        login.setVisible(true);
        String usuario = login.getUsuario();
        String password = login.getContrasena();
        rol_usuario = login.getTipo_usuario();
        //consulta. El ? es para meter datos a la consulta
        String sql = "SELECT * FROM login WHERE " +
                "nombre = ? AND password = ? AND rol_usuario = ?";
        try {
            //CREAR LA SENTENCIA Y LE PASO LOS PARAMETROS
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, usuario);
            sentencia.setString(2, password);
            sentencia.setString(3, rol_usuario);
            //LANZO LA CONSULTA. TE DEVUELVE UN OBJETO RESULTSET
            ResultSet resultado = sentencia.executeQuery();
            //PARA IR AL PRIMER RESULTADO
            if (!resultado.next()) {
                JOptionPane.showMessageDialog(null, "Usuario/Contrasena incorrectos", "Error", JOptionPane.ERROR_MESSAGE);
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }
    public Connection conectar() throws ClassNotFoundException, SQLException {
        //TODO CARGAR EL ARCHIVO DE CONFIGURACION
        if (TIPOCONEXION.equals("mysql")) {
            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection("jdbc:mysql://"+RUTABD+":3306/"+NOMBREBD, "root", "");
        }
        else{
            Class.forName("org.postgresql.Driver");
            conexion = DriverManager.getConnection("jdbc:postgresql://"+RUTABD+":5432/"+NOMBREBD, "root", "");
        }
        return conexion;
    }
    public void agregarNuevoCliente(Cliente cliente, Connection conexion){
        String sql = "INSERT INTO clientes (nombre,apellidos,fecha_nacimiento,domicilio,tipo_cliente)"+
                "VALUES(?,?,?,?,?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, cliente.getNombre());
            sentencia.setString(2, cliente.getApellidos());
            sentencia.setDate(3, new Date(cliente.getFechaNacimiento().getTime()));
            sentencia.setString(4, cliente.getDireccion());
            sentencia.setString(5, cliente.getTipo_Cliente());

            sentencia.executeUpdate();
        } catch (SQLException e1) {
            System.out.println("Error");
        }
    }
    public void agregarNuevoArticulo(Articulo articulo, Connection conexion){
        String sql = "INSERT INTO productos (nombre,descripcion,precio,descuento,cantidad)"+
                "VALUES(?,?,?,?,?)";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, articulo.getNombreArticulo());
            sentencia.setString(2, articulo.getDescripcion());
            sentencia.setFloat(3, articulo.getPrecioArticulo());
            sentencia.setBoolean(4, articulo.isDescuentoArticulo());
            sentencia.setInt(5, articulo.getCantidadArticulos());

            sentencia.executeUpdate();
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }
    public ArrayList<Cliente> obtenerNombreCliente(Connection conexion){
        String sql = "SELECT nombre FROM clientes";
        PreparedStatement sentencia = null;
        ArrayList clientes = new ArrayList();
        Cliente cliente = null;
        try {
            sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();
            while (resultado.next()) {

                cliente = new Cliente();
                cliente.setNombre(resultado.getString(1));
                clientes.add(cliente);
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        return clientes;
    }
    public ArrayList<Articulo> obtenerNombreArticulo(Connection conexion){
        String sql = "SELECT nombre FROM productos";
        PreparedStatement sentencia = null;
        ArrayList articulos = new ArrayList();
        Articulo articulo = null;
        try {
            sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();
            while (resultado.next()) {

                articulo = new Articulo();
                articulo.setNombreArticulo(resultado.getString(1));
                articulos.add(articulo);
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        return articulos;
    }
    public ArrayList<Cliente> refrescarTablaClientes(Connection conexion){
        String sql = "SELECT * FROM clientes";
        PreparedStatement sentencia = null;
        ArrayList clientes = new ArrayList();
        Cliente cliente = null;
        try {
            sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();
            while (resultado.next()) {
                cliente = new Cliente();
                cliente.setId_cliente(resultado.getInt(1));
                cliente.setNombre(resultado.getString(2));
                cliente.setApellidos(resultado.getString(3));
                cliente.setFechaNacimiento(resultado.getDate(4));
                cliente.setDireccion(resultado.getString(5));
                cliente.setTipo_Cliente(resultado.getString(6));
                clientes.add(cliente);
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        return clientes;
    }
    public ArrayList<Articulo> refrescarTablaProductos(Connection conexion){
        String sql = "SELECT * FROM productos";
        PreparedStatement sentencia = null;
        ArrayList articulos = new ArrayList();
        Articulo articulo = null;
        try {
            sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();
            while (resultado.next()) {
                articulo = new Articulo();
                articulo.setId_articulo(resultado.getInt(1));
                articulo.setNombreArticulo(resultado.getString(2));
                articulo.setDescripcion(resultado.getString(3));
                articulo.setPrecioArticulo(resultado.getFloat(4));
                articulo.setDescuentoArticulo(resultado.getBoolean(5));
                articulo.setCantidadArticulos(resultado.getInt(6));
                articulos.add(articulo);
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        return articulos;
    }
    public ArrayList<Factura> refrescarTablaFacturas(Connection conexion){
        String sql = "SELECT * FROM facturas";
        PreparedStatement sentencia = null;
        ArrayList facturas = new ArrayList();
        Factura factura = null;
        try {
            sentencia = conexion.prepareStatement(sql);
            ResultSet resultado = sentencia.executeQuery();
            while (resultado.next()) {
                factura = new Factura();
                factura.setIdFactura(resultado.getInt(1));
                factura.setDomicilioFactura(resultado.getString(2));
                factura.setFechaFactura(resultado.getDate(3));
                factura.setDestinatarioFactura(resultado.getString(4));
                facturas.add(factura);
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        return facturas;
    }
    public int agregarNuevaFactura(Factura factura, Connection conexion, ArrayList<Carrito> listaArticulosPorCliente){
        String altaFactura = "INSERT INTO facturas (domicilio,fecha_factura,destinatario_factura)"+
                "VALUES(?,?,?)";
        String altaProductoFactura = " INSERT INTO factura_carrito ( id_factura ,"+
        "id_producto,total) VALUES (?,?,?)";
        int idFactura = 0;
        try {
            // Inicia transaccion
            conexion.setAutoCommit(false);
            PreparedStatement sentenciaAltaFactura = conexion.prepareStatement(altaFactura, PreparedStatement.RETURN_GENERATED_KEYS);
            sentenciaAltaFactura.setString(1, factura.getDomicilioFactura());
            sentenciaAltaFactura.setDate(2, new Date(factura.getFechaFactura().getTime()));
            sentenciaAltaFactura.setString(3, factura.getDestinatarioFactura());
            sentenciaAltaFactura.executeUpdate();
            // Obtiene el id del producto que se acaba de registrar
            ResultSet idsGenerados = sentenciaAltaFactura.getGeneratedKeys();
            idsGenerados.next();
            idFactura = idsGenerados.getInt(1);
            idsGenerados.close();
            sentenciaAltaFactura.close();
            PreparedStatement sentenciaAtaCarrito = conexion.prepareStatement(altaProductoFactura);
            for(int i = 0 ; i < listaArticulosPorCliente.size(); i++) {
                sentenciaAtaCarrito.setInt(1, idFactura);
                sentenciaAtaCarrito.setInt(2, listaArticulosPorCliente.get(i).getIdArticulo());
                sentenciaAtaCarrito.setFloat(3, listaArticulosPorCliente.get(i).getTotal());
                sentenciaAtaCarrito.executeUpdate();
            }
            // Valida transaccion
            conexion.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idFactura;
    }
    public void eliminarCliente(Cliente c1, Connection conexion) {
        String sentenciaSql = "DELETE FROM clientes WHERE nombre = ? AND " +
                "tipo_cliente = ?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, c1.getNombre());
            sentencia.setString(2, c1.getTipo_Cliente());
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void eliminarProducto(Articulo c1, Connection conexion) {
        String sentenciaSql = "DELETE FROM productos WHERE id = ?";
        PreparedStatement sentencia = null;
        try {
            System.out.println(c1.getNombreArticulo());
            System.out.println(c1.getPrecioArticulo());
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1,c1.getId_articulo());
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public void modificarCliente(Cliente cliente, Connection conexion, String nombreAntiguo){
        String sql = "UPDATE clientes SET nombre = ?, apellidos = ?, fecha_nacimiento = ? " +
                ", domicilio = ?, tipo_cliente = ? WHERE nombre = ?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, cliente.getNombre());
            sentencia.setString(2, cliente.getApellidos());
            sentencia.setDate(3, new Date(cliente.getFechaNacimiento().getTime()));
            sentencia.setString(4, cliente.getDireccion());
            sentencia.setString(5, cliente.getTipo_Cliente());
            sentencia.setString(6, nombreAntiguo);
            sentencia.executeUpdate();
        } catch (SQLException e1) {
            System.out.println("Error al actualizar la tabla Clientes");
        }
    }
    public void modificarArticulo(Articulo articulo, Connection conexion, String nombreAntiguo){
        String sql = "UPDATE productos SET nombre = ?, descripcion = ?, precio = ?, descuento = ?, cantidad = ? WHERE nombre = ?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, articulo.getNombreArticulo());
            sentencia.setString(2, articulo.getDescripcion());
            sentencia.setFloat(3, articulo.getPrecioArticulo());
            sentencia.setBoolean(4, articulo.isDescuentoArticulo());
            sentencia.setInt(5, articulo.getCantidadArticulos());
            sentencia.setString(6, nombreAntiguo);
            sentencia.executeUpdate();
        } catch (SQLException e1) {
            System.out.println("Error al actualizar la base de datos");
        }
    }
    public int funcionCantidadArticulosDescuento(String nombreFuncion, Connection conexion){
        PreparedStatement sentencia = null;
        int cantidad = 0;
        try {
            sentencia = conexion.prepareStatement("select " + nombreFuncion + "()");
            ResultSet resultado = sentencia.executeQuery();
            resultado.next();

            cantidad = resultado.getInt(1);

            resultado.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cantidad;
    }
    public String funcionNombreDomicilioFactura(String nombreFuncion, Connection conexion, String nombre_destinatario){
        PreparedStatement sentencia = null;
        String domicilio = "";
        try {
            sentencia = conexion.prepareStatement("select " + nombreFuncion + "(?)");
            sentencia.setString(1, nombre_destinatario);
            ResultSet resultado = sentencia.executeQuery();
            resultado.next();

            domicilio = resultado.getString(1);

            resultado.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return domicilio;
    }
    public void procedimientoEliminarTipoCliente(String nombreProcedimiento, Connection conexion, String nombreTipoCliente){
        CallableStatement procedimiento = null;
        try{
            procedimiento = conexion.prepareCall("{ call " + nombreProcedimiento + "(?) }");
            procedimiento.setString(1,nombreTipoCliente);
            procedimiento.execute();
        } catch (SQLException e) {
            System.out.println("Error al ejecutar el procedimiento");
        }
    }
    public void procedimientoAplicarDescuentoArticulos(String nombreProcedimiento, Connection conexion){
        CallableStatement procedimiento = null;
        try{
            procedimiento = conexion.prepareCall("{ call " + nombreProcedimiento + "() }");
            procedimiento.execute();
        } catch (SQLException e) {
            System.out.println("Error al ejecutar el procedimiento");
        }
    }
    public float totalCarrito(String nombreFuncion, Connection conexion, int idFactura){
        float total = 0;
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement("select " + nombreFuncion + "(?)");
            sentencia.setInt(1, idFactura);
            ResultSet resultado = sentencia.executeQuery();
            resultado.next();

            total = resultado.getFloat(1);

            resultado.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return total;
    }
    public Articulo devolverDatosProducto(String nombreArticulo,int cantidad, Connection conexion){
        String sql = "SELECT id,precio FROM productos WHERE " +
                "nombre = ? ";
        Articulo articulo = new Articulo();
        try {
            //CREAR LA SENTENCIA Y LE PASO LOS PARAMETROS
            PreparedStatement sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, nombreArticulo);
            //LANZO LA CONSULTA. TE DEVUELVE UN OBJETO RESULTSET
            ResultSet resultado = sentencia.executeQuery();
            //PARA IR AL PRIMER RESULTADO
            resultado.next();
            articulo.setId_articulo(resultado.getInt(1));
            articulo.setPrecioArticulo((resultado.getFloat(2) * cantidad));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return articulo;
    }
    public void modificarTablaCarrito(Connection conexion, int idFactura, int idArticulo){
        String sql = "DELETE FROM factura_carrito WHERE id_factura = ? AND id_producto = ?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, idFactura);
            sentencia.setInt(2, idArticulo);
            sentencia.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (sentencia != null) {
                try {
                    sentencia.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    public ArrayList<Articulo> refrescarTablaCarrito(int idFactura, Connection conexion){
        String sql = "SELECT P.id, P.nombre, P.precio FROM productos P, factura_carrito FC " +
                "WHERE FC.id_factura = ? AND FC.id_producto = P.id";
        PreparedStatement sentencia = null;
        ArrayList listaCarrito = new ArrayList();
        Articulo articulo = null;
        try {
            sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, idFactura);
            ResultSet resultado = sentencia.executeQuery();
            while (resultado.next()) {
                articulo = new Articulo();
                articulo.setId_articulo(resultado.getInt(1));
                articulo.setNombreArticulo(resultado.getString(2));
                articulo.setPrecioArticulo(resultado.getFloat(3));
                listaCarrito.add(articulo);
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        return listaCarrito;
    }
    public void eliminarFactura(Factura factura, Connection conexion){
        String borrarFactura = "DELETE FROM facturas WHERE id = ? AND " +
                "destinatario_factura = ?";;
        String borrarCarrito = " DELETE FROM factura_carrito WHERE id_factura = ?";
        int idFactura = 0;
        try {
            // Inicia transaccion
            conexion.setAutoCommit(false);
            PreparedStatement sentenciaBajaFactura = conexion.prepareStatement(borrarFactura, PreparedStatement.RETURN_GENERATED_KEYS);
            sentenciaBajaFactura.setInt(1, factura.getIdFactura());
            System.out.println(factura.getIdFactura());
            System.out.println(factura.getDestinatarioFactura());
            sentenciaBajaFactura.setString(2, factura.getDestinatarioFactura());
            sentenciaBajaFactura.executeUpdate();
            sentenciaBajaFactura.close();
            PreparedStatement sentenciaBajaCarrito = conexion.prepareStatement(borrarCarrito);
                sentenciaBajaCarrito.setInt(1, factura.getIdFactura());
                sentenciaBajaCarrito.executeUpdate();
            // Valida transaccion
            conexion.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public ArrayList<Cliente> busquedaCliente(Connection conexion, String nombreCliente, String tipoCliente){
        String sql = "SELECT * FROM clientes WHERE nombre = ? OR tipo_cliente = ?";
        PreparedStatement sentencia = null;
        ArrayList clientes = new ArrayList();
        Cliente cliente = null;
        try {
            sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, nombreCliente);
            sentencia.setString(2, tipoCliente);
            ResultSet resultado = sentencia.executeQuery();
            while (resultado.next()) {
                cliente = new Cliente();
                cliente.setNombre(resultado.getString(2));
                cliente.setApellidos(resultado.getString(3));
                cliente.setFechaNacimiento(resultado.getDate(4));
                cliente.setDireccion(resultado.getString(5));
                cliente.setTipo_Cliente(resultado.getString(6));
                clientes.add(cliente);
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        return clientes;
    }
    public ArrayList<Articulo> busquedaArticulo (Connection conexion, String nombreArticulo, float precioArticulo){
        String sql = "SELECT * FROM productos WHERE nombre = ? OR precio = ?";
        PreparedStatement sentencia = null;
        ArrayList articulos = new ArrayList();
        Articulo articulo = null;
        try {
            sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, nombreArticulo);
            sentencia.setFloat(2, precioArticulo);
            ResultSet resultado = sentencia.executeQuery();
            while (resultado.next()) {
                articulo = new Articulo();
                articulo.setNombreArticulo(resultado.getString(2));
                articulo.setDescripcion(resultado.getString(3));
                articulo.setPrecioArticulo(resultado.getFloat(4));
                articulo.setDescuentoArticulo(resultado.getBoolean(5));
                articulo.setCantidadArticulos(resultado.getInt(6));
                articulos.add(articulo);
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        return articulos;
    }
    public ArrayList<Factura> busquedaFactura(Connection conexion, String destinatarioFactura, int idFactura){
        String sql = "SELECT * FROM facturas WHERE id = ? OR destinatario_factura = ?";
        PreparedStatement sentencia = null;
        ArrayList facturas = new ArrayList();
        Factura factura = null;
        try {
            sentencia = conexion.prepareStatement(sql);
            sentencia.setInt(1, idFactura);
            sentencia.setString(2, destinatarioFactura);
            ResultSet resultado = sentencia.executeQuery();
            while (resultado.next()) {
                factura = new Factura();
                factura.setIdFactura(resultado.getInt(1));
                factura.setDomicilioFactura(resultado.getString(2));
                factura.setFechaFactura(resultado.getDate(3));
                factura.setDestinatarioFactura(resultado.getString(4));
                facturas.add(factura);
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        return facturas;
    }
    public void modificarFactura(Factura factura, Connection conexion, int idFactura){
        String sql = "UPDATE facturas SET domicilio = ?, fecha_factura = ?, destinatario_factura = ? " +
                "WHERE id = ?";
        PreparedStatement sentencia = null;
        try {
            sentencia = conexion.prepareStatement(sql);
            sentencia.setString(1, factura.getDomicilioFactura());
            System.out.println(factura.getDomicilioFactura());
            sentencia.setDate(2, new Date(factura.getFechaFactura().getTime()));
            sentencia.setString(3, factura.getDestinatarioFactura());
            sentencia.setInt(4, idFactura);
            System.out.println(idFactura);
            sentencia.executeUpdate();
        } catch (SQLException e1) {
            System.out.println("Error");
        }
    }
    public int funcionDevolverIdProducto(String nombreFuncion,Connection conexion,int idFactura, float precioF){
        PreparedStatement sentencia = null;
        int idArticulo = 0;
        try {
            sentencia = conexion.prepareStatement("select " + nombreFuncion + "(?,?)");
            sentencia.setInt(1, idFactura);
            sentencia.setFloat(2, precioF);
            ResultSet resultado = sentencia.executeQuery();
            resultado.next();

            idArticulo = resultado.getInt(1);

            resultado.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idArticulo;
    }
    public ArrayList<Carrito> devolverDatosCarrito(Connection conexion){
        PreparedStatement sentencia = null;
        Carrito carrito = null;
        ArrayList<Carrito>listaCarritos = new ArrayList<>();
        try {
            sentencia = conexion.prepareStatement("SELECT * FROM factura_carrito");
            ResultSet resultado = sentencia.executeQuery();
            while(resultado.next()){
                carrito = new Carrito();
                carrito.setIdArticulo(resultado.getInt(1));
                carrito.setIdFactura(resultado.getInt(2));
                carrito.setTotal(resultado.getFloat(3));
                listaCarritos.add(carrito);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listaCarritos;
    }
    public void agregarClientes(Connection conexion,ArrayList<Cliente>listaClientes){
        for(int i = 0 ; i < listaClientes.size(); i++) {
            String sql = "INSERT INTO clientes (nombre,apellidos,fecha_nacimiento,domicilio,tipo_cliente)" +
                    "VALUES(?,?,?,?,?)";
            PreparedStatement sentencia = null;
            try {
                sentencia = conexion.prepareStatement(sql);
                sentencia.setString(1, listaClientes.get(i).getNombre());
                sentencia.setString(2, listaClientes.get(i).getApellidos());
                sentencia.setDate(3, new Date(listaClientes.get(i).getFechaNacimiento().getTime()));
                sentencia.setString(4, listaClientes.get(i).getDireccion());
                sentencia.setString(5, listaClientes.get(i).getTipo_Cliente());

                sentencia.executeUpdate();
            } catch (SQLException e1) {
                System.out.println("Error");
            }
        }
    }
    public void agregarArticulos(Connection conexion,ArrayList<Articulo>listaArticulos){
        for(int i = 0 ; i < listaArticulos.size(); i++) {
            String sql = "INSERT INTO productos (nombre,descripcion,precio,descuento,cantidad)" +
                    "VALUES(?,?,?,?,?)";
            PreparedStatement sentencia = null;
            try {
                sentencia = conexion.prepareStatement(sql);
                sentencia.setString(1, listaArticulos.get(i).getNombreArticulo());
                sentencia.setString(2, listaArticulos.get(i).getDescripcion());
                sentencia.setFloat(3, listaArticulos.get(i).getPrecioArticulo());
                sentencia.setBoolean(4, listaArticulos.get(i).isDescuentoArticulo());
                sentencia.setInt(5, listaArticulos.get(i).getCantidadArticulos());

                sentencia.executeUpdate();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
    }
    public ArrayList<Factura> agregarCarritos(Connection conexion, ArrayList<Factura> listaFacturas, ArrayList<Carrito> listaArticulosPorCliente){
        String altaFactura = "INSERT INTO facturas (domicilio,fecha_factura,destinatario_factura)"+
                "VALUES(?,?,?)";
        String altaProductoFactura = " INSERT INTO factura_carrito ( id_factura ,"+
                "id_producto,total) VALUES (?,?,?)";
        int idFactura = 0;
        try {
            // Inicia transaccion
            conexion.setAutoCommit(false);
            for (int i = 0; i < listaFacturas.size(); i++) {
                PreparedStatement sentenciaAltaFactura = conexion.prepareStatement(altaFactura, PreparedStatement.RETURN_GENERATED_KEYS);
                sentenciaAltaFactura.setString(1, listaFacturas.get(i).getDomicilioFactura());
                sentenciaAltaFactura.setDate(2, new Date(listaFacturas.get(i).getFechaFactura().getTime()));
                sentenciaAltaFactura.setString(3, listaFacturas.get(i).getDestinatarioFactura());
                sentenciaAltaFactura.executeUpdate();
                // Obtiene el id del producto que se acaba de registrar
                ResultSet idsGenerados = sentenciaAltaFactura.getGeneratedKeys();
                idsGenerados.next();
                idFactura = idsGenerados.getInt(1);
                idsGenerados.close();
                sentenciaAltaFactura.close();
            }
            PreparedStatement sentenciaAtaCarrito = conexion.prepareStatement(altaProductoFactura);
            for(int i = 0 ; i < listaArticulosPorCliente.size(); i++) {
                sentenciaAtaCarrito.setInt(1, idFactura);
                sentenciaAtaCarrito.setInt(2, listaArticulosPorCliente.get(i).getIdArticulo());
                sentenciaAtaCarrito.setFloat(3, listaArticulosPorCliente.get(i).getTotal());
                sentenciaAtaCarrito.executeUpdate();
            }
            // Valida transaccion
            conexion.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return listaFacturas;
    }
    
}
